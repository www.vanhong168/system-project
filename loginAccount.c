#include <stdio.h>
#include <string.h>
struct Admin {
    char u_name[20];
    char password[20];
} ad;
struct Teacher {
    int id;
    char name[20];
    char sex;
    char dob[20];
    char absent[20];
    char address[20];
} obj[50];
struct Student {
    int id;
    char name[20];
    char sex;
    char dob[20];
    char absent[20];
    char address[20];
} stu[50];
//Student Input
void stuInput(int n) {
	int i;
	for(i=0;i<n;i++){
		printf("ID:");scanf("%d",&stu[i].id);
    	printf("Enter Name: "); scanf("%s", stu[i].name);
    	printf("Enter Sex: "); scanf(" %c", &stu[i].sex);
    	printf("Enter Date of Birth: "); scanf("%s", stu[i].dob);
    	printf("Enter Address: "); scanf("%s", stu[i].address);
    	printf("Enter Absent: "); scanf("%s", stu[i].absent);
	}
}
//teacher Input
void input(int n) {
	int i;
	for(i=0;i<n;i++){
		printf("ID:");scanf("%d",&obj[i].id);
    	printf("Enter Name: "); scanf("%s", obj[i].name);
    	printf("Enter Sex: "); scanf(" %c", &obj[i].sex);
    	printf("Enter Date of Birth: "); scanf("%s", obj[i].dob);
    	printf("Enter Address: "); scanf("%s", obj[i].address);
    	printf("Enter Absent: "); scanf("%s", obj[i].absent);
	}
}
//function for output or read data in Teacher
void output(int n) {
	int i;
    for(i=0;i<n;i++){
    	printf("%d %s %c %s %s %s\n",obj[i].id, obj[i].name, obj[i].sex, obj[i].dob, obj[i].absent,obj[i].address);
	}
}
//student output
void stuOutput(int n) {
	int i;
    for(i=0;i<n;i++){
    	printf("%d %s %c %s %s %s\n",stu[i].id, stu[i].name, stu[i].sex, stu[i].dob, stu[i].absent,stu[i].address);
	}
}
//function for Update data
void update(){
	int teacherId;
    printf("Enter teacher ID to update: ");
    scanf("%d", &teacherId);
	FILE *create = fopen("create.bin", "r");
    if (create == NULL) {
        printf("File is Empty\n");
        return;
    }
	FILE *update = fopen("Update.bin", "w");
    if (update == NULL) {
        printf("Error\n");
        fclose(create);
        return;
    }
	int found = 0;
    struct Teacher temp;
    while (fread(&temp, sizeof(struct Teacher), 1, create)==1) {
        if (obj->id == teacherId) {
			printf("ID:");scanf("%d",&temp.id);
    		printf("Enter Name: "); scanf("%s", &temp.name);
    		printf("Enter Sex: "); scanf(" %c", &temp.sex);
    		printf("Enter Date of Birth: "); scanf("%s",&temp.dob);
    		printf("Enter Address: "); scanf("%s", &temp.address);
    		printf("Enter Absent: "); scanf("%s", &temp.absent);
            // Assuming input() correctly updates the teacher's information
            fwrite(&temp, sizeof(struct Teacher), 1, update);
            found = 1;
            printf("Success\n");
            break;
        } else {
            fwrite(&obj, sizeof(struct Teacher), 1, update);
        }
    }
    fclose(update);
    fclose(create);
    if (!found) {
        printf("Teacher with ID %d not found\n", teacherId);
        remove("Update.bin");
    } else {
        remove("create.bin");
        rename("Update.bin", "create.bin");
    }
}
//update for student
void stuUpdate(){
	int stuId;
    printf("Enter Student ID to update: ");
    scanf("%d", &stuId);
	FILE *stucreate = fopen("stucreate.bin", "r");
    if (stucreate == NULL) {
        printf("File is Empty\n");
        return;
    }
	FILE *update = fopen("Update.bin", "w");
    if (update == NULL) {
        printf("Error\n");
        fclose(stucreate);
        return;
    }
	int found = 0;
    struct Student temp;
    while (fread(&temp, sizeof(struct Student), 1, stucreate)==1) {
        if (stu->id == stuId) {
			printf("ID:");scanf("%d",&temp.id);
    		printf("Enter Name: "); scanf("%s", &temp.name);
    		printf("Enter Sex: "); scanf(" %c", &temp.sex);
    		printf("Enter Date of Birth: "); scanf("%s",&temp.dob);
    		printf("Enter Address: "); scanf("%s", &temp.address);
    		printf("Enter Absent: "); scanf("%s", &temp.absent);
            // Assuming input() correctly updates the teacher's information
            fwrite(&temp, sizeof(struct Student), 1, update);
            found = 1;
            printf("Success\n");
            break;
        } else {
            fwrite(&stu, sizeof(struct Student), 1, update);
        }
    }
    fclose(update);
    fclose(stucreate);
    if (!found) {
        printf("Teacher with ID %d not found\n", stuId);
        remove("Update.bin");
    } else {
        remove("stucreate.bin");
        rename("Update.bin", "stucreate.bin");
    }
}
//function for login account
void loginAccount() {
    int i, op, n;
    FILE *login;
    FILE *create;
    login = fopen("Create_Account.bin", "r");
    if (login == NULL) {
        printf("File is Empty\n");
        return;
    }

    fread(&ad, sizeof(struct Admin), 1, login);
    fclose(login);

    char e_user[20], e_pass[20];
    printf("Enter Username: "); scanf("%s", e_user);
    printf("Enter Password: "); scanf("%s", e_pass);

    if (strcmp(e_user, ad.u_name) == 0 && strcmp(e_pass, ad.password) == 0) {
        printf("Login Success\n");
        do {
            printf("\t1. TEACHER\n");
            printf("\t2. STUDENT\n");
            printf("\t0. EXIT\n");
            printf("Choose Option: "); scanf("%d", &op);
            switch (op) {
                case 1: {
                    int opt;
                    printf("Teacher\n");
                    do {
                        printf("\t1. Create\n");
                        printf("\t2. Output\n");
                        printf("\t3. Delete\n");
                        printf("\t4. Update\n");
                        printf("\t5. Search\n");
                        printf("Please Choose Option: "); scanf("%d", &opt);
                        switch (opt) {
                            case 1: {
                                printf("\tCreate\n");
                                create = fopen("create.bin", "w");
                                if (create == NULL) {
                                    printf("Error\n");
                                } else {
                                    printf("Enter Number of Teachers: "); scanf("%d",&n);
                                    input(n);
                                    fwrite(&obj, sizeof(struct Teacher), n, create);
                                    fclose(create);
                                    printf("Create Success\n");
                                }
                                break;
                            }
                            case 2: {
                                printf("\tOutput\n");
                                printf("Read Data Success\n");
                                FILE *outcreate;
                                outcreate = fopen("create.bin","r");
                                if (outcreate == NULL) {
                                    printf("Error\n");
                                } else {
                                    fread(&obj, sizeof(struct Teacher), n, outcreate);
                                    output(n);
                                    fclose(outcreate);
                                }
                                break;
                            }
                        	case 4: {
    							update();
							}break;
							case 3: {
    							int b = 0;
    							FILE *create = fopen("create.bin", "rb");
    							if (create == NULL) {
        							printf("File is Empty\n");
    							} else {
        						FILE *Delete = fopen("DeleteFile.bin", "wb");
        						if (Delete == NULL) {
            						printf("Error creating DeleteFile.bin\n");
        						} else {
            					struct Teacher obj;
            					int idd;
            					printf("Enter ID of teacher to delete: ");
            					scanf("%d", &idd);
            					while (fread(&obj, sizeof(struct Teacher), 1, create) == 1) {
                					if (obj.id != idd) {
                    					fwrite(&obj, sizeof(struct Teacher), 1, Delete);
                					} else {
                    					printf("Delete Success...\n");
                					}
            					}
            					b = 1;
            					n--;
            					if (b == 0) {
                					printf("Delete Fail: Teacher with given ID not found\n");
            					}
            					fclose(Delete);
        						}
        						fclose(create);
        						remove("create.bin");
        						rename("DeleteFile.bin", "create.bin");
    							}
    							break;
							}
							case 5: {
								int idd,b=0;
								printf("Enter TeacherID to search:");scanf("%d",&idd);
                                printf("\tSearch\n");
                                FILE *outcreate;
                                outcreate = fopen("create.bin","r");
                                if (outcreate == NULL) {
                                    printf("Error\n");
                                } else {
                                    fread(&obj, sizeof(struct Teacher), n, outcreate);
                                    if(idd==obj[i].id){
                                    	output(n);
                                    	b=1;
                                    	printf(" Search Success\n");
									}else{
										printf("Search not found\n");
									}
                                    fclose(outcreate);
                                }     
                            }break;
                        }
                    } while (opt != 0);    
                }break;
                case 2: {
                    int opt;
                    printf("Student\n");
                    do {
                        printf("\t1. Create\n");
                        printf("\t2. Output\n");
                        printf("\t3. Delete\n");
                        printf("\t4. Update\n");
                        printf("\t5. Search\n");
                        printf("Please Choose Option: "); scanf("%d", &opt);
                        switch(opt){
                        	case 1: {
                                printf("\tCreate\n");
                                FILE *stucreate;
                                stucreate = fopen("stucreate.bin", "w");
                                if (stucreate== NULL) {
                                    printf("Error\n");
                                } else {
                                    printf("Enter Number of Student: "); scanf("%d",&n);
                                    stuInput(n);
                                    fwrite(&stu, sizeof(struct Student), n, stucreate);
                                    fclose(stucreate);
                                    printf("Create Success\n");
                                }  
                            }break;
                            case 2: {
                                printf("\tOutput\n");
                                printf("Read Data Success\n");
                                FILE *outcreate;
                                outcreate = fopen("stucreate.bin","r");
                                if (outcreate == NULL) {
                                    printf("Error\n");
                                } else {
                                    fread(&stu, sizeof(struct Student), n, outcreate);
                                    stuOutput(n);
                                    fclose(outcreate);
                                }    
                            }break;
                            case 3: {
    							int b = 0;
    							FILE *stucreate= fopen("stucreate.bin", "rb");
    							if (create == NULL) {
        							printf("File is Empty\n");
    							} else {
        						FILE *Delete = fopen("DeleteFile.bin", "wb");
        						if (Delete == NULL) {
            						printf("Error creating DeleteFile.bin\n");
        						} else {
            					struct Student stu;
            					int idd;
            					printf("Enter ID of Student to delete: ");
            					scanf("%d", &idd);
            					while (fread(&stu, sizeof(struct Student), 1, stucreate) == 1) {
                					if (stu.id != idd) {
                    					fwrite(&stu, sizeof(struct Student), 1, Delete);
                					} else {
                    					printf("Delete Success...\n");
                					}
            					}
            					b = 1;
            					n--;
            					if (b == 0) {
                					printf("Delete Fail: Student with given ID not found\n");
            					}
            					fclose(Delete);
        						}
        						fclose(stucreate);
        						remove("stucreate.bin");
        						rename("DeleteFile.bin", "stucreate.bin");
    							}
							}break;
							case 4: {
    							stuUpdate();
							}break;
							case 5: {
								int idd,b=0;
								printf("Enter StudentID to search:");scanf("%d",&idd);
                                printf("\tSearch\n");
                                FILE *outcreate;
                                outcreate = fopen("stucreate.bin","r");
                                if (outcreate == NULL) {
                                    printf("Error\n");
                                } else {
                                    fread(&stu, sizeof(struct Student), n, outcreate);
                                    if(idd==stu[i].id){
                                    	stuOutput(i);
                                    	b=1;
                                    	printf(" Search Success\n");
									}else{
										printf("Search not found\n");
									}
                                    fclose(outcreate);
                                }   
                            }break;
						}
					}while(opt!=0);
                }break;
                case 0: {
                    printf("Thank You Admin!\n");
                    break;
                }
            }
        } while (op != 0);
    } else {
        printf("Login Fail\n");
    }
}

int main() {
    do {
        loginAccount();
    } while (1);
    return 0;
}

